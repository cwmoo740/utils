if [ -d "node_modules" ]; then
	export PATH=${PATH}:${PWD}/node_modules/.bin
fi

export PATH=${PATH}:${HOME}/.local/bin
export PATH=${PATH}:${HOME}/.config/yarn/global/node_modules/.bin
export PATH=${PATH}:${HOME}/.anaconda/python/bin

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/cowmoo/.google/google-cloud-sdk/path.bash.inc' ]; then source '/home/cowmoo/.google/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/cowmoo/.google/google-cloud-sdk/completion.bash.inc' ]; then source '/home/cowmoo/.google/google-cloud-sdk/completion.bash.inc'; fi
